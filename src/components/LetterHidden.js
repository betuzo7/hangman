import React from 'react';

function LetterHidden(props) {
  return (
    <div className="col s12 m1">
      <p className={`z-depth-5 shadow-demo ${props.class}`}><h2>{props.value}</h2></p>
    </div>
  );
}

export default LetterHidden;