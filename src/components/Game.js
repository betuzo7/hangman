import React from 'react';
import {withRouter} from 'react-router-dom';
import KeyBoard from '../components/KeyBoard';

class Game extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      game: {}
    };
  }

  render() {
    return (
      <div>
	      <div className='home-container'>
	        <h1>Game.</h1>
        </div>
        <div>
          Images
        </div>
        <div>
          Word
        </div>
        <KeyBoard keys={this.state.game.keys}/>
      </div>
    )
  }
}

export default withRouter(Game);