import React from 'react';
import GameListRow from './GameListRow';

const GameList = ({games}) => {
  return (
    <table className="table">
      <thead>
        <tr>
          <th>Name</th>
          <th>Attempt Fails</th>
          <th>Attempt Oks</th>
          <th>Status</th>
        </tr>
      </thead>
      <tbody>
        {games.map(game =>
          <GameListRow key={game.id} game={game} />
        )}
      </tbody>
    </table>
  );
};

export default GameList;