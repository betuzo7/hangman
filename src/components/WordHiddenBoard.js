import React from 'react';
import LetterHidden from '../components/LetterHidden';

class WordHiddenBoard extends React.Component {
  createWordHidden = () => {
		const letters = this.props.letters.split('').map((letter, index) => {
      return <LetterHidden key={index} value={letter} class={letter==='X' ? 'hidden' : 'ok'}/>
    });
    return letters
  }

  render() {
    return (
      <div className='row'>
        {this.createWordHidden()}
      </div>
    );
  }
}

export default WordHiddenBoard;