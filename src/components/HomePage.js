import React from 'react';
import {connect} from 'react-redux';
import GameList from './GameList';

class HomePage extends React.Component {
  render() {
    const games = this.props.games;
    return (
      <div className="col-md-12">
        <GameList games={games} />
      </div>
    );
  }
}

function mapStateToProps(state, ownProps) {
  return {
    games: state.games
  };
}

export default connect(mapStateToProps)(HomePage);