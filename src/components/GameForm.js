import React from 'react';

class GameForm extends React.Component {
  render() {
	  return (
			<div>
	      <h1>New Game.</h1>
	      <form>
	        <div className='form-group'>
	          <label htmlFor='wordHidden'>Word Hidden</label>
	          <input
	            id='wordHidden'
	            name='wordHidden'
	            className='form-control'
	            placeholder='word hidden'
	            type='password'
	            value={this.props.game.wordHidden}
	            autoComplete='off'
	            onChange={this.props.onChange}
	          />
	        </div>
	        <div className='form-group'>
	          <label htmlFor='maxTime'>Max time per turn</label>
	          <select
	            className='form-control'
	            id='maxTime'
	            name='maxTime'
	            value={this.props.game.maxTime}
	            onChange={this.props.onChange}
	          >
	            <option value="NONE">None</option>
	            <option value="SECS5">5 sec</option>
	            <option value="SECS10">10 sec</option>
	            <option value="SECS20">20 sec</option>
	            <option value="SECS30">30 sec</option>
	          </select>
	        </div>
	        <input
	          type="submit"
	          disabled={this.props.saving}
	          value={this.props.saving ? 'Saving...' : 'Save'}
	          className="btn btn-primary"
	          onClick={this.props.onSave}/>
	      </form>
	    </div>
	  )
  }
}

export default GameForm;