import React from 'react';

function Key(props) {
  return (
    <button key={props.id} className={props.classSel} onClick={props.onClick}>
      {props.value}
    </button>
  );
}

export default Key;