import React from 'react';
import {
	Link
}
from 'react-router-dom';

const GameListRow = ({game}) => {
  return (
    <tr>
      <td><Link to={'/game/' + game.id}>{game.letters}</Link></td>
      <td>{game.fails}</td>
      <td>{game.oks}</td>
      <td>{game.statusGame}</td>
    </tr>
  );
};

export default GameListRow;