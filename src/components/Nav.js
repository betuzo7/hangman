import React from 'react';
import {
	Link
}
from 'react-router-dom';

function Nav () {
  return (
    <nav>
	    <ul className='navbar-nav'>
	      <li className='nav-item'>
	        <Link className='nav-link' to='/'>Home</Link>
	      </li>
	      <li className='nav-item'>
	        <Link className='nav-link' to='/game/new'>New Game</Link>
	      </li>
	      <li className='nav-item'>
	        <Link className='nav-link' to='/record'>Records</Link>
	      </li>
	      <li className='nav-item'>
	        <Link className='nav-link'   to='/about'>About</Link>
	      </li>
	    </ul>
    </nav>
  )
}

export default Nav;