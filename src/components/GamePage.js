import React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import KeyBoard from '../components/KeyBoard';
import ImageBoard from '../components/ImageBoard';
import WordHiddenBoard from '../components/WordHiddenBoard';
import * as gameActions from '../actions/gameActions';

class GamePage extends React.Component {

	constructor(props) {
	  super(props);
	  this.saveGameAttempt = this.saveGameAttempt.bind(this);
	}

	generateKeys() {
		const abc = "A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z"
    const keys = abc.split(',').map((key) => {
      const attempt = this.props.game.attempts.find( attempt => attempt.letter === key )
      return {value: key, status: attempt ? attempt.ok ? 1 : 2 : 0}
    });
    return keys;
  }

  saveGameAttempt(event) {
    event.preventDefault();
    const attempt = this.props.game.attempts.find( attempt => attempt.letter === event.target.textContent )
    if (attempt || this.props.game.statusGame !== 'GAMING') {
			return;
    }
    this.props.actions.createGameAttempt({id: this.props.game.id, letter: event.target.textContent});
  }

  render() {
    return (
      <div className="row">
        <ImageBoard  fails={this.props.game.fails} />
        <WordHiddenBoard letters={this.props.game.letters} />
        <KeyBoard keys={this.generateKeys()} onClick={this.saveGameAttempt}/>
      </div>
    )
  }
}

function mapStateToProps(state, ownProps) {
  return {
    game: state.games.find( game => game.id+'' === ownProps.match.params.id )
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(gameActions, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(GamePage);