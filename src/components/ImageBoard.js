import React from 'react';
import logo from '../static/hangman.png';

class ImageBoard extends React.Component {
  render() {
    return (
      <div className='hangman-img'>
        <img src={logo} className={'attempt-' + this.props.fails} alt='Hangman' />
      </div>
    );
  }
}

export default ImageBoard;