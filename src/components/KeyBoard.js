import React from 'react';
import Key from '../components/Key';

class KeyBoard extends React.Component {
	renderSquare = (letter, index) => {
    let className = 'key';
    className += letter.status === 0 ? '' : letter.status === 1 ? ' ok' : ' fail';

    return (
      <Key
        key={index}
        value={letter.value}
        classSel={className}
        onClick={this.props.onClick}
      />
    );
  }

  createTable = () => {
    let table = []
    let children = []
    const keys = this.props.keys.map((key, index) => {
      return this.renderSquare(key, index)
    });

    for (let iCont = 0; iCont < keys.length; iCont++){
      children.push(keys[iCont])
      if ((iCont + 1) % 7 === 0 || iCont === (keys.length - 1)) {
        table.push(<div key={''+iCont} className="board-row">{children}</div>)
        children = []
      }
    }
    return table
  }

  render() {
    return (
      <div>
        {this.createTable()}
      </div>
    );
  }
}

export default KeyBoard;