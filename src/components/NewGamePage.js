import React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as gameActions from '../actions/gameActions';
import GameForm from './GameForm';

class NewGamePage extends React.Component {
	constructor(props) {
	  super(props);
	  this.state = {
	    game: {wordHidden: '', maxTime: 'NONE'},
	    saving: false
	  };
	  this.saveGame = this.saveGame.bind(this);
	  this.updateGameState = this.updateGameState.bind(this);
	}

  updateGameState(event) {
    const field = event.target.name;
    const game = this.state.game;
    game[field] = event.target.value;
    return this.setState({game: game});
  }

  saveGame(event) {
    event.preventDefault();
    this.props.actions.createGame(this.state.game);
  }

  render() {
    return (
      <div>
        <GameForm
          game={this.state.game}
          onSave={this.saveGame}
          onChange={this.updateGameState}/>
      </div>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(gameActions, dispatch)
  };
}

export default connect(null, mapDispatchToProps)(NewGamePage);