import * as types from './actionTypes';
import gameApi from '../api/GameApi';

export function loadGamesSuccess(games) {
  return {type: types.LOAD_GAMES_SUCCESS, games};
}

export function updateGameSuccess(game) {
  return {type: types.UPDATE_GAME_SUCCESS, game}
}

export function createGameSuccess(game) {
  return {type: types.CREATE_GAME_SUCCESS, game}
}

export function deleteGameSuccess(game) {
  return {type: types.DELETE_GAME_SUCCESS, game}
}

export function createGameAttemptSuccess(game) {
  return {type: types.CREATE_GAME_ATTEMPT_SUCCESS, game}
}

export function loadGames() {
  // make async call to api, handle promise, dispatch action when promise is resolved
  return function(dispatch) {
    return gameApi.getAllGames().then(games => {
      dispatch(loadGamesSuccess(games));
    }).catch(error => {
      throw(error);
    });
  };
}

export function updateGame(game) {
  return function (dispatch) {
    return gameApi.updateGame(game).then(responseGame => {
      dispatch(updateGameSuccess(responseGame));
    }).catch(error => {
      throw(error);
    });
  };
}

export function createGame(game) {
  return function (dispatch) {
    return gameApi.createGame(game).then(responseGame => {
      dispatch(createGameSuccess(responseGame));
      return responseGame;
    }).catch(error => {
      throw(error);
    });
  };
}

export function createGameAttempt(attempt) {
  return function (dispatch) {
    return gameApi.createGameAttempt(attempt).then(responseGame => {
      dispatch(createGameAttemptSuccess(responseGame));
      return responseGame;
    }).catch(error => {
      throw(error);
    });
  };
}

export function deleteGame(game) {
  return function(dispatch) {
    return gameApi.deleteGame(game).then(() => {
      console.log(`Deleted ${game.id}`)
      dispatch(deleteGameSuccess(game));
      return;
    }).gamech(error => {
      throw(error);
    })
  }
}

