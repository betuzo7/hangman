import * as types from './actionTypes';
import recordApi from '../api/RecordApi';

export function loadRecordsSuccess(hobbies) {
  return {type: types.LOAD_RECORDS_SUCCESS, hobbies};
}

export function loadRecords() {
  // make async call to api, handle promise, dispatch action when promise is resolved
  return function(dispatch) {
    return recordApi.getAllRecords().then(hobbies => {
      dispatch(loadRecordsSuccess(hobbies));
    }).catch(error => {
      throw(error);
    });
  };
}