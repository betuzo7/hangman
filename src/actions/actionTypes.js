export const LOAD_GAMES_SUCCESS = 'LOAD_GAMES_SUCCESS';
export const LOAD_RECORDS_SUCCESS = 'LOAD_RECORDS_SUCCESS';
export const UPDATE_GAME_SUCCESS = 'UPDATE_GAME_SUCCESS';
export const CREATE_GAME_SUCCESS = 'CREATE_GAME_SUCCESS';
export const CREATE_GAME_ATTEMPT_SUCCESS = 'CREATE_GAME_ATTEMPT_SUCCESS';
export const DELETE_GAME_SUCCESS = 'DELETE_GAME_SUCCESS';