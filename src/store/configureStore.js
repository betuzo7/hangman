import {createStore, applyMiddleware} from 'redux';
//import reduxImmutableStateInvariant from 'redux-immutable-state-invariant';
import thunk from 'redux-thunk';
import HangmanApp from '../reducers'

export default function configureStore() {
  return createStore(
    HangmanApp,
    applyMiddleware(thunk)
  );
}