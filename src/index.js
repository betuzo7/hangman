import React from 'react';
import ReactDOM from 'react-dom';
import configureStore from './store/configureStore';
import {Provider} from 'react-redux'
import { Router } from 'react-router-dom';
import App from './containers/App';
import {loadGames} from './actions/gameActions';
import history from './history'

import './index.css';
import registerServiceWorker from './registerServiceWorker';

const store = configureStore();
store.dispatch(loadGames());

ReactDOM.render(
	<Provider store={store}>
    <Router history={history}>
      <App />
    </Router>
  </Provider>,
document.getElementById('container'));
registerServiceWorker();
