const baseUrl = 'http://localhost:8090/';

class RecordApi {
  static getAllHobbies() {
    return fetch(baseUrl + 'api/v1/record').then(response => {
      return response.json()
    }).catch(error => {
      return error
    });
  }
}

export default RecordApi;