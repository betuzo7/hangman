const baseUrl = 'http://localhost:8090/';

class GameApi {
  static getAllGames() {
    return fetch(baseUrl + 'api/v1/public/game').then(response => {
      return response.json();
    }).catch(error => {
      return error;
    });
  }

  static updateGame(game) {
    const request = new Request(baseUrl + `api/v1/public/game/${game.id}`, {
      method: 'PUT',
      headers: new Headers({
        'Content-Type': 'application/json'
      }),
      body: JSON.stringify({game: game})
    });


    return fetch(request).then(response => {
      return response.json();
    }).catch(error => {
      return error;
    });
  }

  static createGameAttempt(attempt) {
    const request = new Request(baseUrl + `api/v1/public/game/${attempt.id}/attempt`, {
      method: 'POST',
      headers: new Headers({
        'Content-Type': 'application/json'
      }),
      body: JSON.stringify(attempt)
    });


    return fetch(request).then(response => {
      return response.json();
    }).catch(error => {
      return error;
    });
  }

  static createGame(game) {
    const request = new Request(baseUrl + 'api/v1/public/game/', {
      method: 'POST',
      headers: new Headers({
        'Content-Type': 'application/json'
      }),
      body: JSON.stringify(game)
    });


    return fetch(request).then(response => {
      return response.json();
    }).catch(error => {
      return error;
    });
  }

  static deleteGame(game) {
    const request = new Request(baseUrl + `api/v1/public/game/${game.id}`, {
      method: 'DELETE'
    });

    return fetch(request).then(response => {
      return response.json();
    }).catch(error => {
      return error;
    });
  }
}

export default GameApi;