import {
	combineReducers
} from 'redux'
import games from './GameReducer'
import records from './RecordReducer'

const HangmanApp = combineReducers({
	games,
	records
})

export default HangmanApp