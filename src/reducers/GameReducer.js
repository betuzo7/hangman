import * as types from '../actions/actionTypes';
import initialState from './initialState';
import history from '../history'

export default function gameReducer(state = initialState.games, action) {
  switch(action.type) {
    case types.LOAD_GAMES_SUCCESS:
     return action.games
    case types.CREATE_GAME_SUCCESS:
      return [
        ...state.filter(game => game.id !== action.game.id),
        Object.assign({}, action.game)
      ]
    case types.UPDATE_GAME_SUCCESS:
      return [
        ...state.filter(game => game.id !== action.game.id),
        Object.assign({}, action.game)
      ]
    case types.CREATE_GAME_ATTEMPT_SUCCESS:
      return [
        ...state.filter(game => game.id !== action.game.id),
        Object.assign({}, action.game)
      ]
    case types.DELETE_GAME_SUCCESS: {
      const newState = Object.assign([], state);
      const indexOfCatToDelete = state.findIndex(game => {return game.id === action.game.id})
      newState.splice(indexOfCatToDelete, 1);
      history.push('/')
      return newState;
    }
    default:
      return state;
  }
}