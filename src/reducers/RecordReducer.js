import * as types from '../actions/actionTypes';
import initialState from './initialState';

export default function recordReducer(state = initialState.records, action) {
  // state variable here reps just an array of courses
  switch(action.type) {
    case types.LOAD_RECORDS_SUCCESS:
      return Object.assign([], state, action.records);
    default:
      return state;
  }
}