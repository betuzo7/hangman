import React, { Component } from 'react';
import './App.css';

import {
	Route,
	Switch,
	withRouter
} from 'react-router-dom';
import HomePage from '../components/HomePage';
import NewGamePage from '../components/NewGamePage';
import Nav from '../components/Nav';
import Record from '../components/Record';
import GamePage from '../components/GamePage';
import About from '../components/About';

class App extends Component {
	constructor(props) {
    super(props);
    this.state = {
      games: [],
    };
  }

  render() {
    return (
      <div>
				<div class="row">
          <Nav />
        </div>
        <div class="row">
	        <Switch>
		        <Route exact path="/" component={HomePage}/>
		        <Route path="/game/new" component={NewGamePage} />
		        <Route path="/game/:id" component={GamePage} />
		        <Route path="/record" component={Record} />
		        <Route path="/about" component={About} />
	        </Switch>
        </div>
      </div>
    );
  }
}

export default withRouter(App);